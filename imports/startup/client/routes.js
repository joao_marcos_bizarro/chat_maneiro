import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

// Import needed templates
import '../../ui/layouts/body/body.js';
import '../../ui/pages/home/home.js';
import '../../ui/pages/not-found/not-found.js';
import '../../ui/pages/chat/chat.js';
import '../../ui/pages/signIn/signIn.js';
import '../../ui/pages/signUp/signUp.js';
import '../../ui/layouts/accounts/accounts.js';

// Set up all routes in the app
FlowRouter.route('/', {
  name: 'App.home',
  action() {
    BlazeLayout.render('App_body', { main: 'App_home' });
  },
});

FlowRouter.notFound = {
  action() {
    BlazeLayout.render('App_body', { main: 'App_notFound' });
  },
};

FlowRouter.route('/chat', {
    name: 'App.chat',
    action() {
        BlazeLayout.render('App_body', { main: 'App_chat' });
    }
});

FlowRouter.route('/signIn', {
    name: 'App.signIn',
    action() {
        BlazeLayout.render('App_accounts', { main: 'App_signIn' });
    }
});

FlowRouter.route('/signUp', {
    name: 'App.signUp',
    action() {
        BlazeLayout.render('App_accounts', { main: 'App_signUp' });
    }
});
