// Register your apis here

import '../../api/links/methods.js';
import '../../api/links/server/publications.js';

import '../../api/messages/methods.js';
import '../../api/messages/server/publications.js';

import '../../api/users/methods.js';
