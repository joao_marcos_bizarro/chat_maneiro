
export const register = {
    name: 'app.signin',

    validate() {

    },

    run(args) {
        Accounts.createUser({email: args.email, username: args.username, password: args.password});
    },

    call(args, callback) {
        const options = {
            returnStubValue: true,
            throwStubExceptions: true
        };

        Meteor.apply(this.name, [args], options, callback);
    }
};

Meteor.methods({
    [register.name]: function(args) {
        register.validate.call(this, args);
        register.run.call(this, args);
    }
});
