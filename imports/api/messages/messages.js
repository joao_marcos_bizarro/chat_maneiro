import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Logs } from '../logs/logs.js';

class MessagesCollection extends Mongo.Collection
{

    insert(document) {
        super.insert(document);
        Logs.insert({
            time: new Date(),
            operation: 'insert',
            coll: 'messages',
            message: document.message
        });
    }

}

export const Messages = new MessagesCollection('messages');