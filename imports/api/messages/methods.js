import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Messages } from './messages.js';

export const publish  = {
    name: 'message.publish',

    validate(args) {
        check(args, {
            message: String,
            time: Date,
            user: Object
        });

        // TODO: Regras de Negócio
    },

    run(args) {
        Messages.insert(args);
    },

    call(args, callback) {
        const options = {
            returnStubValue: true,
            throwStubExceptions: true
        };

        Meteor.apply(this.name, [args], options, callback);
    }
};

export const remove = {
    name: 'message.remove',

    validate(args) {
        check(args, {
            _id: String
        });

        // TODO: Regras de Negócio
    },

    run(args) {
        Messages.remove({_id: args._id});
    },

    call(args, callback) {
        const options = {
            returnStubValue: true,
            throwStubExceptions: true
        };

        Meteor.apply(this.name, [args], options, callback);
    }
};

Meteor.methods({
    [publish.name]: function (args) {
        publish.validate.call(this, args);
        publish.run.call(this, args);
    },

    [remove.name]: function (args) {
        remove.validate.call(this, args);
        remove.run.call(this, args);
    }
});