import './body.html';

Template.App_body.helpers({
    isLogged: () => {
        return Meteor.userId() ? true : FlowRouter.go('App.signIn');
    }
});