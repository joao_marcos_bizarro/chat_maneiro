import { chai } from 'meteor/practicalmeteor:chai';
import { Template } from 'meteor/templating';
import { $ } from 'meteor/jquery';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { check } from 'meteor/check';
import { _ } from 'meteor/underscore';
import { withRenderedTemplate } from '../../../test_helpers.js';

if(Meteor.isClient) {
    require('../chat.js');
    describe('Chat', function () {

        describe('Helpers', function () {

            it('Should have all helpers', function () {
                withRenderedTemplate('App_chat', {}, (el) => {
                    chai.assert.isTrue(Template.App_chat.__helpers.has('messages'));
                });
            });

            it('Should show messages', function () {
                _.times(3, () => {
                    $('textarea').val('Teste');
                    Template.App_chat.fireEvent('click .btn_publish');
                });
                withRenderedTemplate('App_chat', {}, (el) => {
                    chai.assert.equal($('.ui.blue.segment').length, 3);
                });
            });

        });

        describe('Events', function () {

            it('Should publish message', function () {
                let expected = 'Teste';
                $('textarea').val(expected);
                Template.App_chat.fireEvent('click .btn_publish');
                withRenderedTemplate('App_chat', {}, (el) => {
                    chai.assert.equal($('.msg').val(), expected);
                });
            });

        });

    });
}