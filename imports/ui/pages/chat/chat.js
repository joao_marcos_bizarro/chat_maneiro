import './chat.html';

import { Meteor } from 'meteor/meteor';
import { $ } from 'meteor/jquery';
import { Session } from 'meteor/session';
import { Tracker } from 'meteor/tracker';
import { Messages } from '../../../api/messages/messages.js';
import { publish, remove } from '../../../api/messages/methods.js';

// Mock
//let messages = [];

Template.App_chat.onCreated(function appChatOnCreated() {
    const handle = this.subscribe('messages.get.all');

    Tracker.autorun(() => {
        if(handle.ready()) {
            let messages = Messages.find();
        }
    });
});

Template.App_chat.onRendered(function appChatOnRendered () {
    // Criar o formulário Semantic
    $('.ui.form')
        .form({
            on: 'blur',
            fields: {
                message: {
                    identifier: 'message',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'O campo {name} é obrigatório!'
                        }
                    ]
                }
            }
        })
    ;
});

Template.App_chat.helpers({
    messages: () => {
        // Mock
        //return Session.get('messages');
        return Messages.find();
    }
});

Template.App_chat.events({
    'click .btn_publish'() {
        //event.preventDefault();
        let values = $('.ui.form').form('get values');

        $('.ui.form').form('validate form');
        if(values.message === '') {
            return false;
        }

        publish.call({message: values.message, time: new Date(), user: Meteor.user()}, (err, res) => {
            if(err) {
                alert(err);
            }
        });

        // Mock
        //messages.push({ message: values.message });
        //Session.set('messages', messages);

        $('.ui.form').form('clear');
    }
});