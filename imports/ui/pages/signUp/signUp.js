import './signUp.html';

import { register } from '../../../api/users/methods.js'

Template.App_signUp.onRendered(function appSignUpOnRendered() {
    $('.frm_signup')
        .form({
            on: 'blur',
            fields: {
                email: {
                    identifier: 'email',
                    rules: [

                    ]
                },

                username: {
                    identifier: 'username',
                    rules: []
                },

                password: {
                    identifier: 'password',
                    rules: []
                }
            }
        })
    ;
});

Template.App_signUp.events({
    'click .btn_register'() {
        let values = $('.frm_signup').form('get values');

        register.call(values, (err, res) => {
            if(!err) {
                alert('Cadastrou legal');
            }
        });
    }
});