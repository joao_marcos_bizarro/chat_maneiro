import './signIn.html';

Template.App_signIn.onRendered(function appSignInOnRendered() {
    $('.frm_signin')
        .form({
            on: 'blur',
            fields: {
                email: {
                    identifier: 'email',
                    rules: []
                },

                password: {
                    identifier: 'password',
                    rules: []
                }
            }
        })
    ;
});

Template.App_signIn.events({
    'click .btn_signin'() {
        let values = $('.frm_signin').form('get values');

        Meteor.loginWithPassword(values.email, values.password, (err, res) => {
            if(!err) {
                Meteor.logoutOtherClients();
                FlowRouter.go('App.chat');
            }
        });
    }
});